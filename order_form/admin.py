from django.contrib import admin
from order_form.models import Order

# Register your models here.
admin.site.register(Order)
