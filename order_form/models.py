from django.db import models
from django.utils import timezone


# Create your models here.
class Order(models.Model):
    company_name = models.CharField(max_length=500)
    company_bin = models.CharField(max_length=500)
    phone = models.CharField(max_length=500)
    city = models.CharField(max_length=500)
    date = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return f"{self.pk} : {self.company_name}"

    class Meta:
        ordering = ['-date']
        verbose_name_plural = 'Orders'
