from order_form.models import Order
from rest_framework import serializers
from django.utils import timezone


class OrderSerializer(serializers.ModelSerializer):
    company_name = serializers.CharField(max_length=500)
    company_bin = serializers.CharField(max_length=500)
    city = serializers.CharField(max_length=500)
    phone = serializers.CharField(max_length=500)
    date = serializers.DateTimeField(default=timezone.now)

    def create(self, validated_data):
        instance = Order.objects.create(**validated_data)
        instance.save()

    class Meta:
        model = Order
        fields = '__all__'
