from rest_framework.routers import DefaultRouter
from order_form import views

router = DefaultRouter()
router.register(r"orders", views.OrdersListView, basename="orders")

urlpatterns = router.urls
