import requests
from rest_framework import status
from rest_framework import viewsets
from rest_framework.response import Response
from order_form.models import Order
from order_form.serializers import OrderSerializer

# Create your views here.


class OrdersListView(viewsets.ModelViewSet):
    queryset = Order.objects.all()
    serializer_class = OrderSerializer

    @staticmethod
    def send_notification(data):
        bot_token = '5287202924:AAFEUwvvVNclox0NzyyyCylituRAsx1YKqg'
        chat_id = '-1001544481571'
        text = f"Название компании: {data['company_name']}\n" \
               f"БИН: {data['company_bin']}\n" \
               f"Город: {data['city']}\n" \
               f"Телефон: {data['phone']}\n"
        url = f"https://api.telegram.org/bot{bot_token}/sendMessage?" \
              f"chat_id={chat_id}&text={text}&parse_mode=HTML"
        requests.get(url)

    def create(self, request, *args, **kwargs):
        order = self.serializer_class(data=request.data)
        if order.is_valid():
            order.create(order.validated_data)
            self.send_notification(order.validated_data)
            return Response({'status': 'created'})
        else:
            return Response(order.errors,
                            status=status.HTTP_400_BAD_REQUEST)
