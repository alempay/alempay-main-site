from django.db import models
from django.utils import timezone


class News(models.Model):
    title = models.CharField(max_length=500)
    main_cover = models.ImageField()
    preview_title = models.CharField(max_length=500)
    preview_cover = models.ImageField()
    content = models.TextField()
    date = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return f"{self.pk} : {self.title}"

    class Meta:
        ordering = ['-date']
        verbose_name_plural = 'News'


class SliderImage(models.Model):
    image = models.ImageField()
    source_url = models.URLField()

    def __str__(self):
        return f"{self.image}"
