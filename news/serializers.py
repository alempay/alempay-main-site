from news.models import News, SliderImage
from rest_framework import serializers
from django.utils import timezone


class NewsSerializer(serializers.ModelSerializer):
    title = serializers.CharField(max_length=500)
    main_cover = serializers.ImageField()
    preview_title = serializers.CharField(max_length=500)
    preview_cover = serializers.ImageField()
    content = serializers.CharField(max_length=1000000)
    date = serializers.DateTimeField(default=timezone.now)

    class Meta:
        model = News
        fields = '__all__'


class SliderImageSerializer(serializers.ModelSerializer):
    image = serializers.ImageField()
    source_url = serializers.URLField()

    class Meta:
        model = SliderImage
        fields = '__all__'
