from rest_framework.routers import DefaultRouter
from news import views

router = DefaultRouter()
router.register(r"news", views.NewsViewSet, basename="news")
router.register(r"slider_images", views.SliderImagesViewSet, basename="slider_images")

urlpatterns = router.urls
