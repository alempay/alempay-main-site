from rest_framework import viewsets
from news.serializers import NewsSerializer, SliderImageSerializer
from news.models import News, SliderImage


class NewsViewSet(viewsets.ModelViewSet):
    queryset = News.objects.all()
    serializer_class = NewsSerializer


class SliderImagesViewSet(viewsets.ModelViewSet):
    queryset = SliderImage.objects.all()
    serializer_class = SliderImageSerializer
