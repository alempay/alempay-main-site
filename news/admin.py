from django.contrib import admin
from news.models import News, SliderImage

# Register your models here.
admin.site.register(News)
admin.site.register(SliderImage)
